
public class LazyDatabaseSingleton {

    private static LazyDatabaseSingleton instance;
    private OurArray ourArray;
    private LazyDatabaseSingleton() {
        this.ourArray = new OurArray(4);
    }

    public static LazyDatabaseSingleton getInstance() {
        if (instance == null) {
            instance = new LazyDatabaseSingleton();
        }
        return instance;
    }

    /**
     * Remove person by its identification Number.
     * @param id of item, we want to remove
     * @return true if removing was successful, otherwise false
     */
    public boolean removePerson(String id){
        Person[] people = ourArray.getItems();
        for (int i = 0; i < people.length; i++) {
            if (people[i].getIdentificationNumber().equals(id)) {
                people[i] = null;
                return true;
            }
        }
        return false;
    }

    public Person getPersonById(String id){
        Person[] people = ourArray.getItems();
        for (int i = 0; i < people.length; i++) {
            if (people[i].getIdentificationNumber().equals(id)) {
                return people[i];
            }
        }
        return null;
    }

    public boolean idExists(String id) {
        Person[] people = ourArray.getItems();
        for (Person person: people) {
            if (person == null) {
                continue;
            }
            if (person.getIdentificationNumber().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public OurArray getOurArray() {
        return ourArray;
    }
}
