public class PersonMissingException extends RuntimeException {
    public PersonMissingException(String errorMessage) {
        super(errorMessage);
    }
}
