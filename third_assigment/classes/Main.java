public class Main {
    public static void main(String[] args) {
        boolean appRunning = true;
        LazyDatabaseSingleton database = LazyDatabaseSingleton.getInstance();

        while (appRunning) {
            System.out.println("Choose your action:\n1)Add new person\n2)Remove person\n3)Search person by his identification number");
            int response = ConsoleIO.getInputInRange(1,3);

            switch (response) {
                case 1:
                    System.out.println("Please type person's 1)First name 2)Last name 3)Identification number");
                    String firstName = ConsoleIO.getTextInput();
                    String lastName = ConsoleIO.getTextInput();
                    String identificationNumber = ConsoleIO.getIdentificationNumberInput();
                    if (database.idExists(identificationNumber)) {
                        throw new IdAlreadyExistsException("Person with that ID already exists!");
                    }
                    Person person = new Person(firstName,lastName, identificationNumber);
                    database.getOurArray().addItem(person);
                    System.out.println("Successfully added new person!");
                    break;
                case 2:
                    System.out.println("Please type a identification number of person you want to remove:");
                    String response_id = ConsoleIO.getTextInput();
                    if (!database.removePerson(response_id)) {
                        throw new PersonMissingException("Person with that ID does not exist.");
                    }else {
                        System.out.println("Successfully removed!");
                    }
                    break;
                case 3:
                    System.out.println("Write persons identification number:");
                    String response_identificationNumber = ConsoleIO.getTextInput();
                    Person searched_person = database.getPersonById(response_identificationNumber);
                    if (searched_person != null) {
                        System.out.println("First name: " + searched_person.getFirstName());
                        System.out.println("Last name: " + searched_person.getLastName());
                        System.out.println("Identification number: " + searched_person.getIdentificationNumber());
                        System.out.println("Year of birth: " + searched_person.getBirthYear());
                    } else {
                        throw new PersonMissingException("Person with that ID does not exist.");
                    }
                    break;
            }

            System.out.println("Do you want to continue?\n 1 - Yes/0 - No");
            int continueResponse = ConsoleIO.getInputInRange(0,1);
            if (continueResponse == 0) {
                appRunning = false;
            }
        }

    }
}