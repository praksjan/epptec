public class Person {
    private String firstName;
    private String lastName;
    private String identificationNumber;

    private int birthYear;

    public Person(String firstName, String lastName, String identificationNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.identificationNumber = identificationNumber;
        this.birthYear = Integer.parseInt("20"+identificationNumber.substring(0,2));
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getIdentificationNumber() {
        return identificationNumber;
    }


    public int getBirthYear() {
        return birthYear;
    }
}
