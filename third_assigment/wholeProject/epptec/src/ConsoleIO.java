/**
 * Provides a clean way of getting input from user with restriction on range, blank answers and another faulty responses
 */
public class ConsoleIO {
    public static int getInputInRange(int lower, int upper) {
        //Never happen to return -1
        int userInput = -1;
        boolean repeat;
        do {
            repeat = false;
            try {
                userInput = Integer.parseInt(System.console().readLine());
            }catch (NumberFormatException ex) {
                System.out.println("Type numeric value please.");
                repeat = true;
            }
            if (userInput < lower || userInput > upper) {
                repeat = true;
                System.out.printf("Please write value from %d, to %d.\n", lower, upper);
        }

        }while(repeat);
        return userInput;
    }

    public static String getTextInput() {
        String response = "";

        boolean repeat;
        do {
            repeat = false;
            response = System.console().readLine();
            if (response == null || response.isEmpty()) {
                System.out.println("Please type a valid information.");
                repeat = true;
            }

        }while(repeat);

        return response;
    }

    public static String getIdentificationNumberInput() {
        String response = "";

        boolean repeat = true;
        //If error occures, just jumps through continue to start of the loop and repeat again.
        while (repeat) {
            response = getTextInput();

            if (!(response.length() == 10 || response.length() == 11)) {
                System.out.println("Invalid format of identification number. Check the size of input.");
                continue;
            }
            //Parse identification number to individual chars and process them
            char[] individualChars = response.toCharArray();
            if (response.length() == 11 && !(individualChars[6] == '/')) {
                System.out.println("Invalid format of identification number. Missing the / symbol.");
                continue;
            }
            int year = 0;
            int months = 0;
            int days = 0;
            int xxxx = 0;
            try {
                year = Integer.parseInt("" + individualChars[0] + individualChars[1]);
                months = Integer.parseInt("" + individualChars[2] + individualChars[3]);
                // check the format of / char
                days = Integer.parseInt("" + individualChars[4] + individualChars[5]);
                if (String.valueOf(individualChars[6]) == "/") {
                    xxxx = Integer.parseInt("" + individualChars[7] + individualChars[8] + individualChars[9] + individualChars[10]);
                } else {
                    xxxx = Integer.parseInt("" + individualChars[6] + individualChars[7] + individualChars[8] + individualChars[9]);
                }
                // ----------------------------
            } catch (NumberFormatException ex) {
                System.out.println("Invalid format of identification number. Input does not contain numbers.");
                continue;
            }
            if (year < 1) {
                System.out.println("Invalid format of identification number. Invalid year value.");
                continue;
            }
            if (months > 13 || months < 0) {
                System.out.println("Invalid format of identification number. Invalid month value.");
                continue;
            }
            if (!DayMonthChecker.checkCorrectDay(days, months)) {
                System.out.println("Invalid format of identification number. Invalid day for a month.");
                continue;
            }
            repeat = false;
        }
        return response;
    }
}
