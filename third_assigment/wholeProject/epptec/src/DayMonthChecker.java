public class DayMonthChecker {
    public static boolean checkCorrectDay(int day, int month) {
        if (month < 1 || month > 12)
            return false;

        switch (month) {
            case 1:
                if (day < 1 || day >31)
                    return false;
                break;
            case 2:
                if (day < 1 || day >28)
                    return false;
                break;
            case 3:
                if (day < 1 || day >31)
                    return false;
                break;
            case 4:
                if (day < 1 || day >30)
                    return false;
                break;
            case 5:
                if (day < 1 || day >31)
                    return false;
                break;
            case 6:
                if (day < 1 || day >30)
                    return false;
                break;
            case 7:
                if (day < 1 || day >31)
                    return false;
                break;
            case 8:
                if (day < 1 || day >31)
                    return false;
                break;
            case 9:
                if (day < 1 || day >30)
                    return false;
                break;
            case 10:
                if (day < 1 || day >31)
                    return false;
                break;
            case 11:
                if (day < 1 || day >30)
                    return false;
                break;
            case 12:
                if (day < 1 || day >31)
                    return false;
                break;
        }
        return true;
    }
}
