import java.lang.reflect.Array;

public class OurArray {

    private Person[] items;

    public OurArray(int length) {
        this.items = new Person[length];
    }
    public Person[] expandArray(Person[] old) {
        Person[] newArray = (Person[]) Array.newInstance(OurArray.class, old.length * 2);
        for (int i = 0; i < old.length; i++) {
            newArray[i] = old[i];
        }
        return newArray;
    }

    public void addItem(Person newItem) {
        for (int i = 0; i < items.length; i++) {
            if (items[i] == null) {
                items[i] = newItem;
            }else {
                if (i == items.length - 1) {
                    // create copy of array with its size doubled
                    items = expandArray(items);
                    //Assign value to freshly created array
                    items[i+1] = newItem;
                }
            }
        }
    }

    public Person[] getItems() {
        return items;
    }
}
