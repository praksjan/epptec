public class IdAlreadyExistsException extends RuntimeException {
    public IdAlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }
}
